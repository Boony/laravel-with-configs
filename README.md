<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## About Laravel 6.* with main config

###Guzzle is a PHP HTTP client library
 You can add Guzzle as a dependency using Composer:
 
    composer require guzzlehttp/guzzle:^6.5
 
 Or just add to composer.json file:   
     
    "guzzlehttp/guzzle": "^6.5",

###You may use Composer to install Telescope into your Laravel project:

    composer require laravel/telescope
    
 After installing Telescope, publish its assets using the `telescope:install` Artisan command. After installing Telescope, you should also run the migrate command:

    php artisan telescope:install
   
    php artisan migrate
    
 When updating Telescope, you should re-publish Telescope's assets:   
    
    php artisan telescope:publish
    
####For more [information](https://laravel.com/docs/7.x/telescope#installation)
   
##Installation `spatie/laravel-permission`
 This package can be used with Laravel 5.8 or higher.
 You can install the package via composer:    
    
    composer require spatie/laravel-permission
 
 Optional: The service provider will automatically get registered. Or you may manually add the service provider in your `config/app.php` file:
```php
    'providers' => [
        // ...
        Spatie\Permission\PermissionServiceProvider::class,
    ];
```
    
 You should publish the migration and the `config/permission.php` config file with:   
    
    php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"
 
 Clear your config cache and Run the migrations
 
    php artisan optimize:clear
    
    php artisan config:clear
       
    php artisan migrate
####More [information](https://docs.spatie.be/laravel-permission/v3/installation-laravel/#)

    
###Laravel IDE Helper Generator   
####Installation
 Require this package with composer using the following command:  
    
    composer require --dev barryvdh/laravel-ide-helper
    
 This package makes use of Laravels package auto-discovery mechanism, which means if you don't install dev dependencies in production, it also won't be loaded.
 
 If for some reason you want manually control this:
- add the package to the `extra.laravel.dont-discover` key in `composer.json`, e.g.
  ```json
  "extra": {
    "laravel": {
      "dont-discover": [
        "barryvdh/laravel-ide-helper",
      ]
    }
  }
  ```
- Add the following class to the `providers` array in `config/app.php`:
  ```php
  Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,
  ```
  If you want to manually load it only in non-production environments, instead you can add this to your `AppServiceProvider` with the `register()` method:
  ```php
  public function register()
  {
      if ($this->app->environment() !== 'production') {
          $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
      }
      // ...
  }
  ```

> Note: Avoid caching the configuration in your development environment, it may cause issues after installing this package; respectively clear the cache beforehand via `php artisan cache:clear` if you encounter problems when running the commands
    
####More [information](https://github.com/barryvdh/laravel-ide-helper/blob/master/README.md)    

###    
    "spatie/laravel-cors": "^1.6",
    "tymon/jwt-auth": "^1.0.0"
